<?php
namespace endlessdreams\taxtools\itis\Helpers;

/**
 * ITIS Json helper class
 *
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 *       
 */
class ItisJsonHelper
{

    protected static $url = 'https://www.itis.gov/ITISWebService/jsonservice/ITISService/';

    /**
     *
     * @param string $url            
     * @return string
     */
    protected static function makeGetCurl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $output = curl_exec($ch);
        curl_close($ch);
        return json_decode($output,true);
    }

    /**
     * getAcceptedNamesFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getAcceptedNamesFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getAcceptedNamesFromTSN?' . http_build_query($params));
    }

    /**
     * getCommentDetailFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getCommentDetailFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getCommentDetailFromTSN?' . http_build_query($params));
    }

    /**
     * getCommonNamesFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getCommonNamesFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getCommonNamesFromTSN?' . http_build_query($params));
    }

    /**
     * getCoreMetadataFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getCoreMetadataFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getCoreMetadataFromTSN?' . http_build_query($params));
    }

    /**
     * getCoverageFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getCoverageFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getCoverageFromTSN?' . http_build_query($params));
    }

    /**
     * getCredibilityRatingFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getCredibilityRatingFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getCredibilityRatingFromTSN?' . http_build_query($params));
    }

    /**
     * getCurrencyFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getCurrencyFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getCurrencyFromTSN?' . http_build_query($params));
    }

    /**
     * getDateDataFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getDateDataFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getDateDataFromTSN?' . http_build_query($params));
    }

    /**
     * getExpertsFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getExpertsFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getExpertsFromTSN?' . http_build_query($params));
    }

    /**
     * getFullRecordFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getFullRecordFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getFullRecordFromTSN?' . http_build_query($params));
    }

    /**
     * getGeographicDivisionsFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getGeographicDivisionsFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getGeographicDivisionsFromTSN?' . http_build_query($params));
    }

    /**
     * getGlobalSpeciesCompletenessFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getGlobalSpeciesCompletenessFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getGlobalSpeciesCompletenessFromTSN?' . http_build_query($params));
    }

    /**
     * getJurisdictionalOriginFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getJurisdictionalOriginFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getJurisdictionalOriginFromTSN?' . http_build_query($params));
    }

    /**
     * getKingdomNameFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getKingdomNameFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getKingdomNameFromTSN?' . http_build_query($params));
    }

    /**
     * getOtherSourcesFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getOtherSourcesFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getOtherSourcesFromTSN?' . http_build_query($params));
    }

    /**
     * getParentTSNFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getParentTSNFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getParentTSNFromTSN?' . http_build_query($params));
    }

    /**
     * getPublicationsFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getPublicationsFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getPublicationsFromTSN?' . http_build_query($params));
    }

    /**
     * getReviewYearFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getReviewYearFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getReviewYearFromTSN?' . http_build_query($params));
    }

    /**
     * getScientificNameFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getScientificNameFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getScientificNameFromTSN?' . http_build_query($params));
    }

    /**
     * getSynonymNamesFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getSynonymNamesFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getSynonymNamesFromTSN?' . http_build_query($params));
    }

    /**
     * getTaxonAuthorshipFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getTaxonAuthorshipFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getTaxonAuthorshipFromTSN?' . http_build_query($params));
    }

    /**
     * getTaxonomicRankNameFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getTaxonomicRankNameFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getTaxonomicRankNameFromTSN?' . http_build_query($params));
    }

    /**
     * getTaxonomicUsageFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getTaxonomicUsageFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getTaxonomicUsageFromTSN?' . http_build_query($params));
    }

    /**
     * getUnacceptabilityReasonFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getUnacceptabilityReasonFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getUnacceptabilityReasonFromTSN?' . http_build_query($params));
    }

    /**
     * getFullHierarchyFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getFullHierarchyFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getFullHierarchyFromTSN?' . http_build_query($params));
    }

    /**
     * getHierarchyDownFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getHierarchyDownFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getHierarchyDownFromTSN?' . http_build_query($params));
    }

    /**
     * getHierarchyUpFromTSN
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getHierarchyUpFromTSN(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getHierarchyUpFromTSN?' . http_build_query($params));
    }

    /**
     * searchForAnyMatch
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function searchForAnyMatch(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'searchForAnyMatch?' . http_build_query($params));
    }

    /**
     * searchForAnyMatchPaged
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function searchForAnyMatchPaged(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'searchForAnyMatchPaged?' . http_build_query($params));
    }

    /**
     * getAnyMatchCount
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getAnyMatchCount(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getAnyMatchCount?' . http_build_query($params));
    }

    /**
     * searchByCommonName
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function searchByCommonName(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'searchByCommonName?' . http_build_query($params));
    }

    /**
     * searchByCommonNameBeginsWith
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function searchByCommonNameBeginsWith(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'searchByCommonNameBeginsWith?' . http_build_query($params));
    }

    /**
     * searchByCommonNameEndsWith
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function searchByCommonNameEndsWith(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'searchByCommonNameEndsWith?' . http_build_query($params));
    }

    /**
     * searchByScientificName
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function searchByScientificName(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'searchByScientificName?' . http_build_query($params));
    }

    /**
     * getITISTerms
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getITISTerms(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getITISTerms?' . http_build_query($params));
    }

    /**
     * getITISTermsFromCommonName
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getITISTermsFromCommonName(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getITISTermsFromCommonName?' . http_build_query($params));
    }

    /**
     * getITISTermsFromScientificName
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getITISTermsFromScientificName(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getITISTermsFromScientificName?' . http_build_query($params));
    }

    /**
     * getTsnByVernacularLanguage
     *
     * @param array $params            
     * @return array <string, mixed>
     */
    public static function getTsnByVernacularLanguage(array $params = [])
    {
        return self::makeGetCurl(self::$url . 'getTsnByVernacularLanguage?' . http_build_query($params));
    }

    /**
     * getCredibilityRatings
     *
     * @return array <string, mixed>
     */
    public static function getCredibilityRatings()
    {
        return self::makeGetCurl(self::$url . 'getCredibilityRatings');
    }

    /**
     * getDescription
     *
     * @return array <string, mixed>
     */
    public static function getDescription()
    {
        return self::makeGetCurl(self::$url . 'getDescription');
    }

    /**
     * getGeographicValues
     *
     * @return array <string, mixed>
     */
    public static function getGeographicValues()
    {
        return self::makeGetCurl(self::$url . 'getGeographicValues');
    }

    /**
     * getJurisdictionValues
     *
     * @return array <string, mixed>
     */
    public static function getJurisdictionValues()
    {
        return self::makeGetCurl(self::$url . 'getJurisdictionValues');
    }

    /**
     * getJurisdictionalOriginValues
     *
     * @return array <string, mixed>
     */
    public static function getJurisdictionalOriginValues()
    {
        return self::makeGetCurl(self::$url . 'getJurisdictionalOriginValues');
    }

    /**
     * getKingdomNames
     *
     * @return array <string, mixed>
     */
    public static function getKingdomNames()
    {
        return self::makeGetCurl(self::$url . 'getKingdomNames');
    }

    /**
     * getLastChangeDate
     *
     * @return array <string, mixed>
     */
    public static function getLastChangeDate()
    {
        return self::makeGetCurl(self::$url . 'getLastChangeDate');
    }

    /**
     * getRankNames
     *
     * @return array <string, mixed>
     */
    public static function getRankNames()
    {
        return self::makeGetCurl(self::$url . 'getRankNames');
    }

    /**
     * getVernacularLanguages
     *
     * @return array <string, mixed>
     */
    public static function getVernacularLanguages()
    {
        return self::makeGetCurl(self::$url . 'getVernacularLanguages');
    }
}