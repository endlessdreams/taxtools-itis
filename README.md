# README #



### What is this repository for? ###

This repository is for utils of 
ITIS (Integrated Taxonomic Information System) web services.

The ITIS Web Services provide the ability to search and retrieve data from ITIS 
by providing access to the data behind the ITIS web site
(https://www.itis.gov/web_service.html)

Description - This lists each service function with its description, inputs, 
outputs, and a sample URL that returns results.
(https://www.itis.gov/ws_description.html)

### Installation ###

The preferred way to install this extension is through composer.

Either run

php composer.phar require --dev --prefer-dist endlessdreams/taxtools-itis
or add

"endlessdreams/taxtools-itis": "*"
to the require-dev section of your composer.json file.

### Usage ###

e.g.
var_dump(ItisJsonHelper::searchByScientificName(['srchKey'=>'Acanthaceae']));

### Who do I talk to? ###

Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>